# Построение разбивки в трубной решетке теплообменника
# Необходимо построить показанное ниже разбиение в трубной решетке теплообменника
# На вход в функцию поступают параметры: Диаметр аппарата, Диаметр труб, Расстояние между трубами
# Дополнительно можно добавить параметры: Диаметр вытеснительной трубы, Кол-во рядов труб

# Пример разбивки: sample.svg
from math import sqrt
import sys


def circle(cx, cy, r, fill, stroke_width, stroke):
    return f'\n<circle cx="{cx}" cy="{cy}" r="{r}" fill="{fill}" stroke-width="{stroke_width}" stroke="{stroke}" />'


def begin_svg(width, height):
    return f'<?xml version="1.0" encoding="UTF-8"?>\n<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="{width}" height="{height}" viewBox="{-width / 2} {-height / 2} {width} {height}">\n<defs>\n</defs>'


def path_horz(radius):
    return f'\n<path d="M{radius + 10},0 L{-(radius + 10)},0" stroke="#CC0000" stroke-width="1" fill="none" />'


def path_vert(radius):
    return f'\n<path d="M0,{radius + 10} L0,{-(radius + 10)}" stroke="#CC0000" stroke-width="1" fill="none" />'


end_svg = '\n</svg>'


class math_tubes():

    def __init__(self, r_form, distance_between_tubes, radius_of_all_tubes, NumberOfTubes):
        self.r_form = (float(r_form)) / 2
        self.Distance_between_tubes = float(distance_between_tubes)
        self.radius_of_all_tubes = (float(radius_of_all_tubes)) / 2
        self.NumberOfTubes = NumberOfTubes

    def tube_sheet_diameter(self, radius):
        return circle(0, 0, radius, 'white', 1, 'black')

    def Form_constraint(self, radius):
        return circle(0, 0, radius, 'white', 1, '#CC0000')

    def paint(self):
        r_tube = float(self.r_form * 1.05)
        file = open('tube.svg', 'w')
        file.write(begin_svg(r_tube * 2.1, r_tube * 2.1))
        file.write(self.tube_sheet_diameter(r_tube))
        file.write(self.Form_constraint(self.r_form))
        file.write(path_horz(r_tube))
        file.write(path_vert(r_tube))

        x = 0
        y = 0
        if int(self.NumberOfTubes) % 2 == 0:
            y = self.radius_of_all_tubes + self.Distance_between_tubes/2
        i = 0
        while True:
            if (int(self.NumberOfTubes)/2 <= i):
                print("Максимальное кол-во трубок")
                break
            h = sqrt(
                (x + (int(self.Distance_between_tubes) / 2)) ** 2 + (y + (int(self.Distance_between_tubes) / 2)) ** 2)
            if h > self.r_form:
                print("Не принадлежит")
                x = 0
                if y < self.r_form:
                    i += 1
                    y += (self.Distance_between_tubes + self.radius_of_all_tubes * 2)
                else:
                    print("break")
                    break
            elif h < self.r_form:
                file.write(circle(x, y, self.radius_of_all_tubes, 'white', 2, "#CC0000"))
                print("1")
                if not (x == 0 and y == 0):
                    file.write(circle(-x, -y, self.radius_of_all_tubes, 'white', 2, "#CC0000"))
                    print("2")
                if x != 0 and y != 0:
                    file.write(circle(-x, y, self.radius_of_all_tubes, 'white', 2, "#CC0000"))
                    file.write(circle(x, -y, self.radius_of_all_tubes, 'white', 2, "#CC0000"))
                    print("3 and 4")
                x += (self.Distance_between_tubes + self.radius_of_all_tubes * 2)
                print("Принадлежит")
        file.write(end_svg)
        file.close()
        print("Запись в файл завершена")


if __name__ == "__main__":
    if type(sys.argv[1]) == int and type(sys.argv[2]) == int and type(sys.argv[3]) == int and type(sys.argv[4]) == int:
        main_date = math_tubes(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
        main_date.paint()
    else:
        print("Введено не число!")
